import React from 'react';

import EmailLogin from "./pages/EmailLogin";

import Home from "./pages/Home";
import Network from "./pages/Network";
import FindPop from "./pages/FindPop";
import Profile from "./pages/Profile";
import PopDetails from "./pages/PopDetails"

import './App.css';
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import LoginLayout from "./layouts/LoginLayout";
import AppLayout from "./layouts/AppLayout";

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout>
      <Component {...props} />
    </Layout>
  )} />
)

function App() {
  return (
    <BrowserRouter>
        <Switch>
          <AppRoute exact path="/" layout={LoginLayout} component={EmailLogin} />
          <AppRoute exact path="/Home" layout={AppLayout} component={Home} />
          <AppRoute exact path="/Network" layout={AppLayout} component={Network} />
          <AppRoute exact path="/Findpop" layout={AppLayout} component={FindPop} />
          <AppRoute exact path="/Profile" layout={AppLayout} component={Profile} />
          <AppRoute exact path="/Network/:id" layout={AppLayout} component={PopDetails} />
        </Switch>
    </BrowserRouter>
    );
}
export default App;
