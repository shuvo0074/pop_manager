import React, { Component } from "react";

import Link from "../../components/HyperLink";
 
class Menu extends Component {
  render() {
    return (
    <ul className="menuHolder">
        <li className="menuItem">
            <Link
            linkTitle="Home"
            linkClass="navLink"
            linkPath="home"
            activeLinkClass="navLinkActive"
            />
        </li>
        <li className="menuItem">
            <Link
            linkTitle="Network"
            linkClass="navLink"
            linkPath="network"
            activeLinkClass="navLinkActive"
            />
        </li>
        <li className="menuItem">
            <Link
            linkTitle="Findpop"
            linkClass="navLink"
            linkPath="findpop"
            activeLinkClass="navLinkActive"
            />
        </li>
        <li className="menuItem">
            <Link
            linkTitle="Profile"
            linkClass="navLink"
            linkPath="profile"
            activeLinkClass="navLinkActive"
            />
        </li>
        <div className="profileLinkHolder spacer">
            <button
            className="barLink btnLogout"
            onClick={this.props.handleLogout}
            >Logout</button>
        </div>
    </ul>
    );
  }
}
 
export default Menu;