import React, { Component, setGlobal } from "reactn";
import { withRouter} from 'react-router-dom';

import Menu from "./Menu";


import "../../styles/Sidebar.css";
 
class Sidebar extends Component {

    handleLogout() {
        localStorage.setItem("token",null);
        setGlobal({loggedIn: false})
        this.props.history.push(`/`);
    }

    render() {
        return (
            <div className="sidebar">
                <div className="profileHolder fullWidthHolder spacer">
                    <div className="profileContent">
                        <h4>Welcome</h4>
                        <h2>User Name</h2>
                    </div>
                </div>
                <div className="fullWidthHolder spacer">
                    <Menu 
                    handleLogout={this.handleLogout.bind(this)}
                    />
                </div>
            </div>
        );
    }
}
 
export default withRouter (Sidebar);