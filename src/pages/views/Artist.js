import React, { Component } from "react";
import { withRouter} from 'react-router-dom';

import Button from "../../components/Button";
import Card from "../../components/CardList";
import CardSingle from "../../components/CardSingle";

import "../../styles/Sidebar.css";

import Data from "../../assets/data.json";
 
class Artist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trending: [],
            artistWeek: [],
            activity: [],
            recent: []
        };
    }
    render() {
        return (
            <div className="pageContent">
                <div className="backButtonHolder">
                    <Button
                        buttonTitle="Back"
                        buttonStyle="link"
                        buttonClick={this.props.history.goBack}
                    />
                </div>
                <div className="column2Holder">
                    <Card
                    cardTitle="Trending"
                    cardData={this.state.trending}
                    cardStyle="vertList"
                    />
                    <CardSingle
                    cardTitle="Artist of the week"
                    cardData={this.state.artistWeek}
                    cardStyle=""
                    />
                </div>
                <div className="rowHolder">
                    <Card
                    cardTitle="Top Songs"
                    cardData={this.state.activity}
                    cardStyle="horList"
                    cardLimit={4}
                    />
                </div>
                <div className="rowHolder">
                    <Card
                    cardTitle="Top Albums"
                    cardData={this.state.trending}
                    cardStyle="horList"
                    cardLimit={4}
                    />
                </div>
            </div>
        );
    }
}
 
export default withRouter (Artist);