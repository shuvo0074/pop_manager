import React, {  useState,setGlobal } from "reactn";
import { withRouter} from 'react-router-dom';

import FormInput from "../components/FormInput"; //For input class
import Button from "../components/Button";

import "../styles/Forms.css";
import FetchService from "../services/FetchService";
 
function EmailLogin (props) {

const [email,setEmail]=useState('')
const [password,setPassword]=useState('')
  function inputEmail(val){
    setEmail(val)
  }
  function inputPassword(val){
    setPassword(val)
  }

    function ButtonClick () {
      console.log('hello');

      let body={
        email,password
      }
      FetchService("POST","rest-auth/login/",body,false)
        .then((response)=>{
          console.log('hello',response.key);
          localStorage.setItem("token", response.key);
          setGlobal({loggedIn: true})
          props.history.push(`/home`);
        })
        .catch((err)=>{
          console.log(err);
      })

    }

    return (
        <div className="loginHolder">

          <div className="loginContentLeft">
            <div className="errorMsg spacer">
                <p className="errorTxt">
                    ISP NTTN POP Network.
                </p>
            </div>
          </div>

          <div className="loginContentRight">

            {/* email starts */}
            <div className="formItem spacer">
                <FormInput
                inputStyle="inputTxt"
                inputType="email"
                inputValue={email}
                inputPlaceholder="Email"
                inputFunction={(val)=>inputEmail(val.target.value)}
              />
            </div>
            {/* email ends */}

            {/* password starts */}
            <div className="formItem spacer">
                <FormInput
                inputStyle="inputTxt"
                inputType="password"
                inputValue={password}
                inputPlaceholder="Password"
                inputFunction={(val)=>inputPassword(val.target.value)}
              />
            </div>
            {/* password ends */}

            {/* button starts */}
            <div className="formItem spacer">
                <Button
                buttonStyle="large"
                buttonTitle="Login"
                buttonState="inactive"
                buttonClick={ButtonClick}
                />
            </div>
            {/* button ends */}
          </div>
				</div>
    );
  }

 
export default withRouter (EmailLogin);