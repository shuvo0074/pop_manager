
import React, { useState , useEffect } from "reactn";
import FetchService from "../services/FetchService";

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { withRouter,NavLink} from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 600,
  },
  head: {
    margin: 20
  }
}); 
function Network () {
  const classes = useStyles();

  const [searchText,setSearchText]=useState('')
  const [result,setResult]=useState()
  const [data,setData]=useState()
  const [division,setDivision] = useState (-1)
  const [ divisions,setDivisions]=useState([])
  const [ district,setDistrict]=useState(-1)
  const [ districts,setDistricts]=useState([])
  const [ type,setType]=useState(-1)
  const [ types, setTypes] = useState([])
  const [ loading, setLoading] = useState(true)

  function filterType(e){
    let {value}=e.target
    setType(value)
    let arr= result
    arr= arr.filter(arrData=>arrData.provider_type.name.toLowerCase().includes(types[value-1].name.toLowerCase()))
    setData(arr)
    setDistrict(-1)
    setDivision(-1)
  }

  function filterDistrict(e){
    let {value}=e.target
    setDistrict(value)
    let arr= result
    arr= arr.filter(arrData=>arrData.district.id==districts[value].id)
    setData(arr)
  }

  function findDistrict(e){
    let {value}=e.target
    setDivision(value)
    FetchService("GET","districts/?division="+value)
    .then(res=>setDistricts(res.data))
    .then(()=>{
      let arr= result
      arr=arr.filter(arrData=>arrData.division.id==divisions[parseInt(value-1)].id)
      setData(arr)
    })
    .catch(()=>{})    
  }

  useEffect(()=>{
    FetchService("GET","divisions/")
    .then(res=>setDivisions(res.data))
    .then(()=>{
      FetchService("GET","isp-types/")
      .then(res=>setTypes(res.data))
      .then(()=>{
        FetchService("GET","pops/?ownpops&provider&isp_type&division=6",{},true)
        .then(resp=>{
          console.log(resp.data)
          setData(resp.data)
          setResult(resp.data)
        })
        .then(()=>setLoading(false))
      })
    })
  },[])
    // const handleOnDragStart = e => e.preventDefault()
    if (loading)
    return <div>
      <h2>loading</h2>
    </div>

    return (
      <Paper className={classes.root}>
       <h1 className={classes.head} >Your Network</h1>
       <div>
       <select
          style={{margin:20}}
          onChange={filterType}
          value={type}
       >
          <option selected value={-1} disabled>Select Type</option>

         {
           types.map((type)=><option value={type.id}>{type.name}</option>)
         }
        </select>

        <select
          style={{margin:20}}
          onChange={findDistrict}
          value={division}
        >
        <option selected value={-1} disabled>Select Division</option>
         {
           divisions.map((divn)=><option value={divn.id}>{divn.name}</option>)
         }
        </select>
        <select
          style={{margin:20}}
          onChange={filterDistrict}
          value={district}
        >
        <option selected value={-1} disabled>{districts.length>0?"Select District":"Select division first"}</option>
         {
           districts.map((distr,index)=><option value={index}>{distr.name}</option>)
         }
        </select>
        <input style={{margin:10}} type="text" value={searchText} onChange={e=>setSearchText(e.target.value)} placeholder="Search... " />
        <button
        onClick={()=>{
          setDistrict(-1)
          setDivision(-1)
          setType(-1)
          setData(result)
        }}
        >
          Reset Filter
        </button>
        </div>
      <TableContainer  className={classes.container}>
      <Table stickyHeader className={classes.table} aria-label="sticky table">
        <TableHead>
          <TableRow>
            <TableCell>POP id</TableCell>
            <TableCell align="right">Type of ISP</TableCell>
            <TableCell align="right">POP address</TableCell>
            <TableCell align="right">Division</TableCell>
            <TableCell align="right">District</TableCell>
            <TableCell align="right">Thana</TableCell>
            <TableCell align="right">Ward</TableCell>
            <TableCell align="right">Latitude</TableCell>
            <TableCell align="right">Longitude</TableCell>
            <TableCell align="right">Contact Info</TableCell>
            <TableCell align="right">Equipment</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row) => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
              <NavLink
              title={row.id}
              to={"/Network/" + row.id}
              >
                #ID: {row.id}
              </NavLink>
              </TableCell>
              <TableCell align="right">{row.provider_type.name}</TableCell>
              <TableCell align="right">{row.pop_address}</TableCell>
              <TableCell align="right">{row.division.name}</TableCell>
              <TableCell align="right">{row.district.name}</TableCell>
              <TableCell align="right">{row.upazilla.name}</TableCell>
              <TableCell align="right">{row.ward}</TableCell>
              <TableCell align="right">{row.lat}</TableCell>
              <TableCell align="right">{row.lon}</TableCell>
              <TableCell align="right">{row.contact_info}</TableCell>
              <TableCell align="right">{row.equipments}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
      </Paper>
    );
  }
 
export default withRouter (Network);