import React, { useState,useEffect } from "reactn";
import "../styles/Profile.css"
import { useParams } from "react-router-dom";
import profileIcon from '../profileIcon.jpg';
import FetchService from "../services/FetchService";

function Profile () {
  let { id } = useParams();
  const [loading,setLoading]=useState(true)
  const [name,setName] = useState('')
  const [ user,setUser] = useState({})
  const [type,setType]=useState('')
  const [types,setTypes]=useState([])
  const [address, setAddress]=useState('')
  const [division,setDivision]=useState('')
  const [divisions,setDivisions]=useState('')
  const [ district,setDistrict ]=useState('')
  const [ districts,setDistricts ]=useState('')
  const [ thana,setThana ]=useState('')
  const [ thanas,setThanas ]=useState('')
  const [ ward ,setWard ]=useState('')
  const [ wards ,setWards ]=useState('')
  const [ latitude,setLatitude ]=useState('')
  const [longitude ,setLongitude ]=useState('')
  const [contact ,setContact ]=useState('')
  const [ equipment,setEquipment ]=useState('')


  useEffect(()=>{
    FetchService("GET","pops/"+id)
    .then(res=>{
      console.log(res)
      setUser(res)
      setName(res.provider_name)
      setType(res.provider_type.id)
      setAddress(res.pop_address)
      setDivision(res.division.id)
      setDistrict(res.district.id)
      setThana(res.upazilla.id)
      setWard(res.ward)
      setLatitude(res.lat)
      setLongitude(res.lon)
      setContact(res.contact_info)
      setEquipment(res.equipments)

      FetchService("GET","divisions/")
      .then(divs=>setDivisions(divs.data))
      .then(()=>FetchService("GET","isp-types/"))
      .then(typs=>setTypes(typs.data))
      .then(()=>FetchService("GET","districts/?division="+res.division.id))
      .then(dists=>setDistricts(dists.data))
      .then(()=>FetchService("GET","upazillas/?division="+res.division.id+"&district="+res.district.id))
      .then(thas=>setThanas(thas.data))
      .then(()=>FetchService("GET","upazillas/?division="+res.division.id+"&district="+res.district.id+"&upazilla="+res.upazilla.id))
      .then(wars=>setWards(wars.data))
      .then(()=>setLoading(false))

    })
    // .then((res)=>console.log(res.division))
    
},[])


    if (loading)
    return (
      <div>
        <h2>
          Loading
        </h2>
      </div>
    )
    return (
        <div className="pageContent">
            <img src={profileIcon} className="profileIconHolder" alt="logo" />
            <p>
              Welcome {'\n'}
              {name}
            </p>
            <select
              style={{margin:20}}
              value={type}
              onChange={e=>setType(e.target.value)}
       >

          <option selected value="Select Type" disabled>Select Type</option>

         {
           types.map((type)=><option value={type.id}>{type.name}</option>)
         }
        </select>
        <input style={{margin:10}} type="text" value={address} onChange={e=>setAddress(e.target.value)} placeholder="Enter POP address" />
        <select
              style={{margin:20}}
              value={division}
              onChange={e=>setDivision(e.target.value)}
       >
          <option selected value="Select Type" disabled>Select Division</option>

         {
           divisions.map((type)=><option value={type.id}>{type.name}</option>)
         }
        </select>
        <select
              style={{margin:20}}
              value={district}
              onChange={e=>setDistrict(e.target.value)}
       >
          <option selected value="Select Type" disabled>Select District</option>

         {
           districts.map((type)=><option value={type.id}>{type.name}</option>)
         }
        </select>
        <select
              style={{margin:20}}
              value={thana}
              onChange={e=>setThana(e.target.value)}
       >
          <option selected value="Select Type" disabled>Select Upazilla</option>

         {
           thanas.map((type)=><option value={type.id}>{type.name}</option>)
         }
        </select>
        <select
              style={{margin:20}}
              value={ward}
              onChange={e=>setWard(e.target.value)}
       >
          <option selected value="Select Type" disabled>Select Ward</option>

         {
           wards.map((type)=><option value={type.id}>{type.name}</option>)
         }
        </select>
        <input style={{margin:10}} type="text" value={latitude} onChange={e=>setLatitude(e.target.value)} placeholder="Latitude " />
        <input style={{margin:10}} type="text" value={longitude} onChange={e=>setLongitude(e.target.value)} placeholder="Longitude... " />
        <input style={{margin:10}} type="text" value={contact} onChange={e=>setContact(e.target.value)} placeholder="Contact Details... " />
        <input style={{margin:10}} type="text" value={equipment} onChange={e=>setEquipment(e.target.value)} placeholder="Equipment... " />
        <button
        onClick={()=>{
          console.log(id,type, address, division, district, thana,ward,latitude,longitude,contact,equipment)
        }}
        >Update</button>
        </div>
    );
  }

export default Profile;