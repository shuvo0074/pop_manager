import React, { Component,useEffect,useState } from "reactn";
import ReactMapboxGl, { Popup, Marker } from 'react-mapbox-gl';
import marker from '../assets/marker.png'
import rowData from '../assets/data.json';
import FetchService from "../services/FetchService";

const Map = ReactMapboxGl({
  accessToken:
    'pk.eyJ1Ijoic2h1dm8wMDc0IiwiYSI6ImNrY3Z6M3g0ZzA5MXAyemtkb3F3MzR0a3EifQ.Ij-0k8L30kXRQ-ae2OuX2w',
    logoPosition:'top-left',
    minZoom:0,
});
function FindPop () {
  const [searchText,setSearchText]=useState('')
  const [result,setResult]=useState(rowData.slice(0,200))
  const [data,setData]=useState(rowData.slice(0,200))
  const [division,setDivision] = useState (-1)
  const [ divisions,setDivisions]=useState([])
  const [ district,setDistrict]=useState(-1)
  const [ districts,setDistricts]=useState([])
  const [ type,setType]=useState(-1)
  const [ types, setTypes] = useState([])
  const [ loading, setLoading] = useState(true)
  const [ lat , setLat ] = useState('')
  const [ lon, setLon ] = useState('')
  // 23.791665 - lat, 90.420293 - lon
  const [ zoom, setZoom] = useState(0)
  
  function filterType(e){
    let {value}=e.target
    setType(value)
    let arr= result
    arr= arr.filter(arrData=>arrData.provider_type.name.toLowerCase().includes(types[value-1].name.toLowerCase()))
    setData(arr)
    setDistrict(-1)
    setDivision(-1)
  }

  function filterDistrict(e){
    let {value}=e.target
    setDistrict(value)
    let arr= result
    arr= arr.filter(arrData=>arrData.district.id==districts[value].id)
    setData(arr)
  }

  function findDistrict(e){
    let {value}=e.target
    setDivision(value)
    FetchService("GET","districts/?division="+value)
    .then(res=>setDistricts(res.data))
    .then(()=>{
      let arr= result
      arr=arr.filter(arrData=>arrData.division.id==divisions[parseInt(value-1)].id)
      setData(arr)
    })
    .catch(()=>{})    
  }

  useEffect(()=>{
    FetchService("GET","divisions/")
    .then(res=>setDivisions(res.data))
    .then(()=>{
      FetchService("GET","isp-types/")
      .then(res=>{
        setTypes(res.data)
        FetchService("GET","pops",{},true)
        .then(resp=>{
          console.log(resp.data)
          setData(resp.data)
          setResult(resp.data)
        })
        .then(()=>setLoading(false))
      })
    })
  },[])

  if (loading)
  return (
    <div>
      <h2>
        Loading
      </h2>
    </div>
  )
    return (
      <div className="pageContent">
        <div>
        <h3>
        Findpop
        </h3>
       <select
          style={{margin:20}}
          onChange={filterType}
          value={type}
       >
          <option selected value={-1} disabled>Select Type</option>

         {
           types.map((type)=><option value={type.id}>{type.name}</option>)
         }
        </select>

        <select
          style={{margin:20}}
          onChange={findDistrict}
          value={division}
        >
        <option selected value={-1} disabled>Select Division</option>
         {
           divisions.map((divn)=><option value={divn.id}>{divn.name}</option>)
         }
        </select>
        <select
          style={{margin:20}}
          onChange={filterDistrict}
          value={district}
        >
        <option selected value={-1} disabled>{districts.length>0?"Select District":"Select division first"}</option>
         {
           districts.map((distr,index)=><option value={index}>{distr.name}</option>)
         }
        </select>
        <input style={{margin:10}} type="text" value={searchText} onChange={ e => setSearchText(e.target.value)} placeholder="Search... " />
        <button
        onClick={()=>{
          setDistrict(-1)
          setDivision(-1)
          setType(-1)
          setLat('')
          setLon('')
          setData(result)
        }}
        >
          Reset Filter
        </button>

        </div>
        <div>
        <input style={{margin:10}} type="number" value={lat} onChange={e=>{
          setLat(e.target.value)
        }} 
        placeholder="latitude... " />
        <input style={{margin:10}} type="number" value={lon} onChange={e=>{
          setLon(e.target.value)
        }} 
        placeholder="longitude... " />
        <button
        onClick={()=>{
          let arr = result
          arr=arr.filter(popData=>(popData.lon>=parseFloat(lon)-0.25 && popData.lon<=parseFloat(lon)+0.25) && (popData.lat>=parseFloat(lat)-0.25 && popData.lat<=parseFloat(lat)+0.25))
            setZoom(0) //set zoom to 6 here
          setData(arr)
        }}
        >
          Find nearest POP
        </button>
        </div>
        <Map
          style="mapbox://styles/mapbox/streets-v9"
          containerStyle={{
            height: '80vh',
            width: '50vw',
            borderWidth:'2px',
          }}
          zoom={[zoom]} // set to 6 for bangladesh
          center={[90.420293,23.791665]}
          onClick={data=>console.log(data)}
        >
          {
            data.map(data=>
              data.lat || data.lon || data.lat!=='' || data.lon!=='' ? 
            <Marker
              coordinates={[data.lon,data.lat]}
              anchor="bottom">
              <img style={{
                height:30,
                width:30
              }} src={marker}/>
            </Marker>
            :<div/>
            )
          }
        </Map>
        <div
        style={{
          flexDirection:'row',
          fontSize:10
        }}
        >
        <img style={{
              height:15,
              width:15
            }} src={marker}/> Icon by  <a href="https://www.flaticon.com/authors/srip" title="srip">srip</a>
            </div>
      </div>
    );
  }
 
export default FindPop;