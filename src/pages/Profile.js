import "../styles/Profile.css"
import React, { useEffect, useState } from "reactn";
import FetchService from "../services/FetchService";
import profileIcon from '../profileIcon.jpg';

function Profile ({navigation}) {
  const [data,setData]= useState({})
  const [loading,setLoading]=useState(true)
  const [name,setName] = useState('')
  const [ user,setUser] = useState({})
  const [providerType,setProviderType]=useState('')
  const [ispType,setIspType]=useState([])
  const [ispTypes,setIspTypes]=useState([])
  const [address, setAddress]=useState('')
  const [division,setDivision]=useState('')
  const [divisions,setDivisions]=useState('')
  const [ district,setDistrict ]=useState('')
  const [ districts,setDistricts ]=useState('')
  const [ latitude,setLatitude ]=useState('')
  const [longitude ,setLongitude ]=useState('')

  // function editProfile() {
  //   this.props.history.push(`/EditProfile`);
  // }
  useEffect(()=>{
    // console.log(navigation.state.params.id)

    FetchService("GET","providers/me/",{},true)
    .then(res=>{
      console.log(res)
      setUser(res)
      setName(res.name)
      setIspType(res.isp_type.id)
      setProviderType(res.provider_type)
      setAddress(res.address)
      setDivision(res.division.id)
      setDistrict(res.district.id)
      setLatitude(res.lat)
      setLongitude(res.lon)

      FetchService("GET","divisions/")
      .then(divs=>setDivisions(divs.data))
      .then(()=>FetchService("GET","isp-types/"))
      .then(typs=>setIspTypes(typs.data))
      .then(()=>FetchService("GET","districts/?division="+res.division.id))
      .then(dists=>setDistricts(dists.data))
      .then(()=>setLoading(false))
    })
  },[navigation])
  if(loading)
  return(
    <div>
      <h2>loading</h2>
    </div>
  )
    return (
    <div className="pageContent">
        <img src={profileIcon} className="profileIconHolder" alt="logo" />
        <p>
          Welcome {'\n'}
          {name}
        </p>
    <input style={{margin:10}} type="text" value={name} onChange={e=>setName(e.target.value)} placeholder="Name " />

    <select
      style={{margin:20}}
      value={ispType}
      onChange={e=>setIspType(e.target.value)}
   >

      <option selected value={-1} disabled>Select ISP Type</option>

     {
       ispTypes.map((type)=><option value={type.id}>{type.name}</option>)
     }
    </select>

    <a>{providerType}</a>

    <input style={{margin:10}} type="text" value={address} onChange={e=>setAddress(e.target.value)} placeholder="Enter POP address" />
    
    <select
          style={{margin:20}}
          value={division}
          onChange={e=>setDivision(e.target.value)}
   >
      <option selected value={-1} disabled>Select Division</option>

     {
       divisions.map((type)=><option value={type.id}>{type.name}</option>)
     }
    </select>

    <select
          style={{margin:20}}
          value={district}
          onChange={e=>setDistrict(e.target.value)}
   >
      <option selected value={-1} disabled>Select District</option>

     {
       districts.map((type)=><option value={type.id}>{type.name}</option>)
     }
    </select>

    <input style={{margin:10}} type="text" value={latitude} onChange={e=>setLatitude(e.target.value)} placeholder="Latitude " />
    <input style={{margin:10}} type="text" value={longitude} onChange={e=>setLongitude(e.target.value)} placeholder="Longitude... " />
    <button
    onClick={()=>{
      console.log(ispType,providerType, address, division, district, name,latitude,longitude)
    }}
    >Update</button>
    </div>
);
    // (
    //   <div className="pageContent">
    //     <h1>
    //       Profile
    //     </h1>
    //     <img src={profileIcon} className="profileIconHolder" alt="logo" />
    //     <h2>First Name</h2>
    //     <p>{data.first_name} </p>
    //     <h2>Last Name</h2>
    //     <p>{data.last_name} </p>
    //     <h2>Email</h2>
    //     <p>{data.email} </p>
    //     <h2>Username</h2>
    //     <p>{data.username} </p>
        
		// 	</div>
    // );
  }
 
export default Profile;