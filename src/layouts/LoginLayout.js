import React, { Component,setGlobal } from "reactn";

import "../styles/Login.css";


class LoginLayout extends Component {
  componentDidMount() {
    if(localStorage.getItem('token') === 'null' || typeof(localStorage.getItem('token')) === 'object') {
      setGlobal({loggedIn: false})
      this.props.children.props.history.push("/")
    } else {
      setGlobal({loggedIn: true})
      this.props.children.props.history.push("/home")
    }
    this.global.loggedIn ? this.props.children.props.history.push("/home") :   this.props.children.props.history.push("/")
  }
  render() {
    return (
        <div id="fullscreen" >
          <div className="contentHolder">
            {this.props.children}
          </div>
        </div>
    );
  }
}

export default LoginLayout;