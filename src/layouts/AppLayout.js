import React, { Component } from "react";

import Sidebar from "../pages/views/Sidebar";
import "../styles/App.css";

class AppLayout extends Component {

  componentDidUpdate() {
    this._div.scrollTop = 0;
  }

  render() {
    return (
        <div id="appScreen">
          <div id="sidebarHolder">
            <div id="sidebarContent">
              <Sidebar />
            </div>
          </div>
	        <div id="appContent" ref={(ref) => this._div = ref}>
            <div className="page">
            {this.props.children}
            </div>
          </div>
				</div>
    );
  }
}

export default AppLayout;