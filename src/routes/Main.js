import React, { Component } from "react";
import {
  Route,
  BrowserRouter,
  Switch
} from "react-router-dom";

import Login from "../pages/Login";
import EmailLogin from "../pages/EmailLogin";
import Signup from "../pages/Signup";
import ForgetPassword from "../pages/ForgetPassword";

import Home from "../pages/Home";
import Bangla from "../pages/Bangla";
import Discover from "../pages/Discover";
import Library from "../pages/Library";
import Profile from "../pages/Profile";
import ProfileEdit from "../pages/ProfileEdit";

import LevelDetail from "../pages/LevelDetail";

import LoginLayout from "../layouts/LoginLayout";
import AppLayout from "../layouts/AppLayout";

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout>
      <Component {...props} />
    </Layout>
  )} />
)
 
class Main extends Component {
  render() {
    return (
    <BrowserRouter>
        <Switch>
          <AppRoute exact path="/" layout={LoginLayout} component={Login} />
          <AppRoute exact path="/login" layout={LoginLayout} component={EmailLogin} />
          <AppRoute exact path="/Signup" layout={LoginLayout} component={Signup} />
          <AppRoute exact path="/forgetpassword" layout={LoginLayout} component={ForgetPassword} />
          <AppRoute exact path="/Home" layout={AppLayout} component={Home} />
          <AppRoute exact path="/Bangla" layout={AppLayout} component={Bangla} />
          <AppRoute exact path="/Discover" layout={AppLayout} component={Discover} />
          <AppRoute exact path="/Library" layout={AppLayout} component={Library} />
          <AppRoute exact path="/Profile" layout={AppLayout} component={Profile} />
          <AppRoute exact path="/EditProfile" layout={AppLayout} component={ProfileEdit} />
          <AppRoute exact path="/:levelType/:levelTitle" layout={AppLayout} component={LevelDetail} />
        </Switch>
    </BrowserRouter>
    );
  }
}
 
export default Main;