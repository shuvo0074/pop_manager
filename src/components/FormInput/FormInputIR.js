import React, { Component } from "react";

import "./Styles.css";
 
class FormInputIR extends Component {

  render() {
    return (
    	<input
            name={this.props.inputName}
            className={this.props.inputStyle}
            type={this.props.inputType}
            checked={this.props.inputChecked}
            data-test={this.props.inputData}
            value={this.props.inputValue}
            onChange={this.props.inputFunction}
            placeholder={this.props.inputPlaceholder}
            autoComplete={this.props.inputAutocomplete}
            disabled={this.props.inputDisabled}
          />
    );
  }
}
 
export default FormInputIR;