import React, { Component } from "react";

import "./Styles.css";
 
class ButtonIR extends Component {

  render() {
    return (
    	<button
        id={this.props.buttonStyle}
        className={this.props.buttonState}
        onClick={this.props.buttonClick}
        >
            <span className="btnTxt">
                {this.props.buttonTitle}
            </span>
        </button>
    );
  }
}
 
export default ButtonIR;