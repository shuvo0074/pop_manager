import React, { Component } from "react";
import {
  NavLink
} from "react-router-dom";

import "./Styles.css";
 
class HyperLink extends Component {

  render() {
    return (
      <NavLink
      title={this.props.linkTitle}
      className={this.props.linkClass}
      to={"/" + this.props.linkPath}
      activeClassName={this.props.activeLinkClass}
      params={{linkTitle:this.props.linkTitle}}
      >
        {this.props.linkTitle}
      </NavLink>
    );
  }
}
 
export default HyperLink;