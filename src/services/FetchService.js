import { BaseUrl } from '../env';
export default async function FetchService(method,type, body={},Auth=true,triedCount=5){
   function errorHandler(err){
      
    let retryCount = triedCount - 1;
    if(!retryCount){
        throw err;
    }
    
    return setDelay(1000).then(() => FetchService(method,type,body,Auth,retryCount));
  }

  function setDelay(d){
    return new Promise((resolve) => setTimeout(resolve, d));
  }

    let u = BaseUrl+type
    
    let headers = method ==="GET" || Auth?
      {
        Authorization: "Token " + localStorage.getItem('token'),
        "Content-Type": "application/json",
        Accept: "*/*"
      }
      :{
        "Content-Type": "application/json",
        Accept: "*/*"
      }
      
      let options = method ==="GET"?
      { method, headers}
      :{ method, headers, body: JSON.stringify(body) }

      return fetch(u,options)
      .then(function(res) {
        if (res.ok) {
          return res;
        }
      })
      .then((data)=>data.json()) //We can handle res.status == 200 && res.ok in this section and do response.json in next promise
      .catch(errorHandler)

}